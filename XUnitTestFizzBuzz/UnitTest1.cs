using System;
using Xunit;
using Xunit.Extensions;
using FizzBuzzProject;

namespace XUnitTestFizzBuzz {
    public class UnitTest1 {
        [Fact]
        public void should_Num() {
            string expected = "1";
            string actual = FizzBuzz.DoFizzOrBuzz(1);
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void should_Fizz() {
            string expected = "Fizz";
            string actual = FizzBuzz.DoFizzOrBuzz(3);
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void should_Buzz() {
            string expected = "Buzz";
            string actual = FizzBuzz.DoFizzOrBuzz(5);
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void should_FizzBuzz() {
            string expected = "FizzBuzz";
            string actual = FizzBuzz.DoFizzOrBuzz(15);
            Assert.Equal(expected, actual);
        }
    }
}
